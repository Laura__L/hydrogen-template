import Layout from '../components/Layout.server';
import RegisterForm from '../components/RegisterForm.client';

export default function Register() {
  return (
    <Layout>
      <RegisterForm />
    </Layout>
  );
}
