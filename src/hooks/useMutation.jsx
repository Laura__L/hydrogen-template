import {graphqlRequestBody} from '@shopify/hydrogen';
import * as React from 'react';
import shopifyConfig from '../../shopify.config';

let headers = {
  'Content-Type': 'application/json',
};

const initialState = {pending: false, error: undefined, data: undefined};

const actions = {
  fetch: 'fetch',
  success: 'success',
  error: 'error',
};

function reducer(state, action) {
  switch (action.type) {
    case 'fetch':
      return {...state, pending: true};
    case 'success':
      console.log('success dispatched');
      return {...state, pending: false, error: null, data: action.payload};
    case 'error':
      return {...state, pending: false, error: action.payload};
    default:
      return state;
  }
}

export default function useMutation() {
  const {storeDomain, storefrontToken} = shopifyConfig;
  const [state, dispatch] = React.useReducer(reducer, initialState);

  headers['X-Shopify-Storefront-Access-Token'] = storefrontToken;

  const executeMutation = async ({query, variables}) => {
    const url = `https://${storeDomain}/api/2022-01/graphql.json`;
    const body = graphqlRequestBody(query, variables);
    const options = {
      headers,
      body,
      method: 'POST',
      mode: 'cors',
    };

    dispatch({type: actions.fetch});
    fetch(url, options)
      .then((res) => res.json())
      .then((data) => {
        data.errors
          ? dispatch({type: actions.error, payload: data.errors})
          : dispatch({type: actions.success, payload: data});
      });
  };

  return {...state, executeMutation};
}
