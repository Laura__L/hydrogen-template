import gql from 'graphql-tag';
import React, {useRef, useState, useEffect} from 'react';
import useMutation from '../hooks/useMutation';
import extractFormValues from '../utils/extractFormValues';
import toCamelCase from '../utils/toCamelCase';
import TextInput from './TextInput.client';
// import {Spinner} from './progress';
// import {checkbox} from './register-form.module.css';
// import {toast} from 'react-toastify';

function RegisterForm() {
  const formRef = useRef(null);
  const successMessage = `Sucessfully registered, please log in`;
  const [errors, setErrors] = useState({});
  const {executeMutation: createCustomer, data, error, pending} = useMutation();
  const inputTexts = ['First name', 'Last name', 'Email', 'Phone', 'Password'];
  const handleSubmit = (e) => {
    const customer = {input: {...extractFormValues(e)}};

    e.preventDefault();
    createCustomer({query, variables: customer});
  };

  const errorsReducer = (object, error) => ({
    ...object,
    [error.field[1]]: error.message,
  });

  useEffect(() => {
    const result = data?.data?.customerCreate || false;
    const handleErrors = (errors) =>
      setErrors(errors.reduce(errorsReducer, {}));

    if (result.customerUserErrors) handleErrors(result.customerUserErrors);
    if (result.customer) {
      //toast(successMessage);
      console.log(successMessage);
    }
  }, [data, successMessage]);

  if (pending) return <div>Loading...</div>;

  return (
    <form className="max-w-xl mx-auto" ref={formRef} onSubmit={handleSubmit}>
      {inputTexts.map((text) => {
        const identifier = toCamelCase(text);
        return (
          <TextInput
            key={identifier}
            {...{text, identifier, error: errors[identifier]}}
          />
        );
      })}

      <label
        className="flex gap-1 mb-8 items-center"
        htmlFor="acceptsMarketing"
      >
        <input
          type="checkbox"
          value="acceptsMarketing"
          name="acceptsMarketing"
          id="acceptsMarketing"
        />
        I wish to recieve marketing communications
      </label>
      <button
        className="block m-0 mx-auto w-full max-w-md items-center justify-center uppercase font-medium text-center px-6 py-4 rounded disabled:border-gray-300 disabled:bg-gray-300 disabled:cursor-not-allowed text-white bg-gray-900 hover:bg-gray-800 active:bg-gray-700"
        type="submit"
      >
        Register
      </button>
      {data?.errors && data?.errors.message}
      {error?.message}
    </form>
  );
}

const query = gql`
  mutation customerCreate($input: CustomerCreateInput!) {
    customerCreate(input: $input) {
      customer {
        firstName
        lastName
        email
        phone
        acceptsMarketing
      }
      customerUserErrors {
        field
        message
        code
      }
    }
  }
`;

export default RegisterForm;
