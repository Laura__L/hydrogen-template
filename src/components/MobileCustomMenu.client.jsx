import {Link} from '@shopify/hydrogen/client';

/**
 * Import a custom menu containing submenus
 */
export default function MobileCustomMenu({data}) {
  const getHandle = (url) => url.split('/').pop();

  const buildMenu = (items, initialLevel = true) => (
    <ul className="md:flex text-left">
      {items.map((menu) => (
        <li
          key={menu.id}
          className={initialLevel ? 'dropdown-mobile' : 'dropdown-mobile mx-7'}
        >
          <Link
            to={`/collections/${getHandle(menu.url)}`}
            className={
              'block p-3 hover:opacity-80' +
              (initialLevel && ' text-lg p-4 font-bold')
            }
          >
            {menu.title}
          </Link>
          <div className={'hidden ' + (initialLevel && 'w-full pt-2 pb-4')}>
            {menu.items && buildMenu(menu.items, false)}
          </div>
        </li>
      ))}
    </ul>
  );

  return <div>{buildMenu(data?.menu.items)}</div>;
}
