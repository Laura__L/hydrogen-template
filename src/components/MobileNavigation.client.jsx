import {Fragment, useEffect} from 'react';
import {FocusTrap} from '@headlessui/react';
import MobileCountrySelector from './MobileCountrySelector.client';
import OpenIcon from './OpenIcon';
import MobileCustomMenu from './MobileCustomMenu.client';
//import CollectionsMenu from './CollenctionsMenu.client';

let scrollPosition = 0;

/**
 * A client component that defines the navigation for a mobile storefront
 */
export default function MobileNavigation({
  //collections,
  isOpen,
  setIsOpen,
  customMenu,
}) {
  const OpenFocusTrap = isOpen ? FocusTrap : Fragment;

  useEffect(() => {
    if (isOpen) {
      scrollPosition = window.scrollY;
      document.body.style.position = 'fixed';
    } else if (document.body.style.position) {
      document.body.style.position = '';
      window.scrollTo(0, scrollPosition);
    }
  }, [isOpen]);

  return (
    <div className="lg:hidden">
      <OpenFocusTrap>
        <button
          type="button"
          className="flex justify-center items-center w-7 h-full"
          onClick={() => setIsOpen((previousIsOpen) => !previousIsOpen)}
        >
          <span className="sr-only">{isOpen ? 'Close' : 'Open'} Menu</span>
          {isOpen ? <CloseIcon /> : <OpenIcon />}
        </button>
        {isOpen ? (
          <div className="fixed -left-0 top-20 w-full h-screen z-10 bg-gray-50 px-4 md:px-12 py-7">
            {/* <CollectionsMenu {...{setIsOpen, collections}} /> */}
            {/* <MobileCustomMenu data={customMenu} /> */}
            <MobileCountrySelector />
          </div>
        ) : null}
      </OpenFocusTrap>
    </div>
  );
}

function CloseIcon() {
  return (
    <svg
      width="18"
      height="18"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1 17L17 1M1 1L17 17"
        stroke="black"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
