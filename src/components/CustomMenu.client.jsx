import {Link} from '@shopify/hydrogen/client';

/**
 * Import a custom menu containing submenus
 */
export default function CustomMenu({data}) {
  const getHandle = (url) => url.split('/').pop();

  const buildMenu = (items, initialLevel = true) => (
    <ul className="hidden lg:flex justify-center items-start">
      {items.map((menu) => (
        <li
          key={menu.id}
          className={
            initialLevel
              ? 'dropdown text-center font-bold'
              : 'dropdown text-center mx-7'
          }
        >
          <Link
            to={`/collections/${getHandle(menu.url)}`}
            className={
              'block p-3 hover:opacity-80' + (initialLevel && ' text-lg p-4')
            }
          >
            {menu.title}
          </Link>
          <div
            className={
              initialLevel &&
              'hidden absolute w-full bg-white left-0 border shadow-sm pt-2 pb-4'
            }
          >
            {menu.items && buildMenu(menu.items, false)}
          </div>
        </li>
      ))}
    </ul>
  );

  return <div>{buildMenu(data?.menu.items)}</div>;
}
