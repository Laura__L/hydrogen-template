import React from 'react';

function TextInput({text, identifier, error}) {
  return (
    <label className="mb-5" htmlFor={identifier}>
      {text}
      <input
        className="block w-full h-12 px-3 border mt-1"
        type="text"
        id={identifier}
        name={identifier}
      />
      <div className="mb-5 text-red-600">{error}</div>
    </label>
  );
}

export default TextInput;
