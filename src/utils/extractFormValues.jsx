export default function extractFormValues(e) {
  const elements = [...e.target.elements];
  const values = elements.reduce((obj, element) => {
    if (!element.name) return obj;

    const value = element.type === 'checkbox' ? element.checked : element.value;
    return {...obj, [element.name]: value};
  }, {});

  return values;
}
